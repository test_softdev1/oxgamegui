/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ananchai.oxgameui;

import static com.ananchai.oxgameui.Player.Xwin;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lenovo
 */
public class TestWriteObject {

    private static Player player;
    
    public static void main(String[] args) {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        player = new Player(); 
        player.xWin();
        player.xLose();
        player.oWin();
        player.oLose();
        player.oxDrew();
        System.out.println(player.XtoString());
        System.out.println(player.OtoString());
        try {
            file = new File("ox.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            
            oos.writeObject(player);
            oos.close();
            fos.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestWriteObject.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestWriteObject.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(TestWriteObject.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
}
